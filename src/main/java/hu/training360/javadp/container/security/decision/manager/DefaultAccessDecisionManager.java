package hu.training360.javadp.container.security.decision.manager;

import java.util.ArrayList;
import java.util.List;

import hu.training360.javadp.container.common.Component;
import hu.training360.javadp.container.security.SecurityContext;
import hu.training360.javadp.container.security.strategy.DecisionStrategy;
import hu.training360.javadp.container.security.voter.FriendlyVoter;
import hu.training360.javadp.container.security.voter.RoleBasedVoter;
import hu.training360.javadp.container.security.voter.Voter;
import hu.training360.javadp.container.security.voter.VoterDecision;

@Component
public class DefaultAccessDecisionManager implements AccessDecisionManager {

	private List<Voter> voters = new ArrayList<>();

	public DefaultAccessDecisionManager() {
		voters.add(new RoleBasedVoter());
		voters.add(new FriendlyVoter());
	}

	@Override
	public boolean isAccessGranted(DecisionStrategy strategy, SecurityContext sc) {

		List<VoterDecision> decisions = new ArrayList<>();

		for (Voter v : voters) {

			decisions.add(v.vote(sc));
		}

		return strategy.isGranted(decisions);
	}

}
