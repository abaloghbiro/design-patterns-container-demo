package hu.training360.training.javadp.container.factory;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.reflections.Reflections;

import hu.training360.javadp.container.common.AbstractProxyFactoryBean;
import hu.training360.javadp.container.common.Component;
import hu.training360.training.javadp.container.bean.BeanDefinition;
import hu.training360.training.javadp.container.bean.Scope;

public class DefaultBeanFactory implements BeanFactory {

	private final Map<Class<? extends Object>, BeanDefinition> BEAN_DEFINITIONS = new HashMap<Class<?>, BeanDefinition>();

	private final Map<Class<? extends Object>, Object> SINGLETONS = new HashMap<Class<? extends Object>, Object>();

	private static final Reflections REF = new Reflections("");

	{
		Set<Class<?>> managedBeans = REF.getTypesAnnotatedWith(Component.class);

		for (Class<?> c : managedBeans) {

			Component component = c.getAnnotation(Component.class);
			BeanDefinition def = new BeanDefinition();
			def.setName(component.name());
			def.setScope(component.scope());
			def.setId(c.getClass().getName());
			def.setType(c);
			BEAN_DEFINITIONS.put(c, def);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T extends Object> T getBean(Class<T> beanClazz) {

		BeanDefinition def = BEAN_DEFINITIONS.get(beanClazz);

		if (def != null) {

			if (Scope.SINGLETON == def.getScope()) {

				if (SINGLETONS.containsKey(beanClazz)) {
					return (T) SINGLETONS.get(beanClazz);
				}
				synchronized (SINGLETONS) {
					try {
						T ret = createAndPerformDependencyInjection(beanClazz);
						SINGLETONS.put(beanClazz, ret);
						return ret;
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			} else if (Scope.PROTOTYPE == def.getScope()) {
				try {
					return createAndPerformDependencyInjection(beanClazz);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			throw new IllegalArgumentException("Invalid bean definition!");
		}
		throw new IllegalArgumentException("No bean definition found!");
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	private <T extends Object> T createAndPerformDependencyInjection(Class<T> type) {

		T ret = null;
		try {
			ret = type.newInstance();
			Field[] fields = type.getDeclaredFields();

			for (Field f : fields) {

				if (f.getAnnotation(Inject.class) != null) {

					T resolvedDependency = null;

					Class<T> dependencyFieldType = (Class<T>) f.getType();

					if (dependencyFieldType.isInterface()) {
						Class<T> implementationClass = getImplementerClassOfTheSpecifiedInterface(dependencyFieldType);
						resolvedDependency = createAndPerformDependencyInjection(implementationClass);
					} else {
						resolvedDependency = createAndPerformDependencyInjection(dependencyFieldType);
					}
					f.setAccessible(true);
					f.set(ret, resolvedDependency);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return ret;

	}

	@SuppressWarnings("unchecked")
	public <T extends Object> T getProxiedObject(T rawObject) {

		List<BeanDefinition> beanDefinitionsOfProxyFactoryBeans = BEAN_DEFINITIONS.values().stream()
				.filter(c -> AbstractProxyFactoryBean.class.isAssignableFrom(c.getType())).collect(Collectors.toList());

		if (!beanDefinitionsOfProxyFactoryBeans.isEmpty()) {

			List<AbstractProxyFactoryBean> factoryBeanInstances = new ArrayList<>();

			for (BeanDefinition def : beanDefinitionsOfProxyFactoryBeans) {
				Class<T> defType = (Class<T>) def.getType();
				factoryBeanInstances.add((AbstractProxyFactoryBean) getBean(defType));
			}

			for (int i = 0; i < factoryBeanInstances.size(); i++) {
				AbstractProxyFactoryBean current = (AbstractProxyFactoryBean) factoryBeanInstances.get(i);
				AbstractProxyFactoryBean next = null;
				if (i != factoryBeanInstances.size() - 1) {
					next = (AbstractProxyFactoryBean) factoryBeanInstances.get(i + 1);
				}
				current.setNextProxyFactoryBean(next);
			}
			AbstractProxyFactoryBean firstInTheChain = (AbstractProxyFactoryBean) factoryBeanInstances.get(0);
			return (T) firstInTheChain.createProxy(rawObject);
		}

		return rawObject;

	}

	@SuppressWarnings("unchecked")
	private <T extends Object> Class<T> getImplementerClassOfTheSpecifiedInterface(Class<T> interfaceClazz) {

		if (interfaceClazz.isInterface()) {
			Set<Class<? extends T>> implementerClasses = REF.getSubTypesOf(interfaceClazz);

			if (implementerClasses.size() == 1) {

				Class<T> implementor = (Class<T>) implementerClasses.iterator().next();
				return implementor;

			} else {
				throw new IllegalStateException(
						"Dependency is ambigous, no or multiple implementation found of " + interfaceClazz);
			}
		}

		throw new IllegalArgumentException("The specified class is not a real interface!");

	}
}
