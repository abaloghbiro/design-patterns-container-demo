package hu.training360.javadp.container.test;

import java.util.Date;

import hu.training360.javadp.container.common.Component;
import hu.training360.javadp.container.event.EventBus;
import hu.training360.javadp.container.event.TextBasedEvent;
import hu.training360.training.javadp.container.factory.Inject;

@Component
public class DefaultTestService2 implements TestService2 {

	@Inject
	private EventBus bus;

	public void test() {
		System.out.println("Test method");
		bus.sendMessage(new TextBasedEvent(this, new Date(), "attila"));
	}

}
