package hu.training360.javadp.container.event;

import java.util.Date;

public class TextBasedEvent extends Event<String> {

	public TextBasedEvent(Object source, Date timestamp, String username) {
		super(source, timestamp, username);
	}

}
