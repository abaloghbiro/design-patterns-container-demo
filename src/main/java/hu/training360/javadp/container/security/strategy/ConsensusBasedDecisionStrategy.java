package hu.training360.javadp.container.security.strategy;

import java.util.List;

import hu.training360.javadp.container.common.Component;
import hu.training360.javadp.container.security.voter.VoterDecision;

@Component
public class ConsensusBasedDecisionStrategy implements DecisionStrategy {

	@Override
	public boolean isGranted(List<VoterDecision> decisions) {

		long countOfDecisions = decisions.size();

		long countOfApprovedDecisions = decisions.stream().filter(d -> d == VoterDecision.APPROVED).count();

		return countOfDecisions == countOfApprovedDecisions;
	}

}
