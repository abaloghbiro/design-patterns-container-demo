package hu.training360.javadp.container.test;

import hu.training360.javadp.container.audit.Loggable;
import hu.training360.javadp.container.security.Secure;
import hu.training360.javadp.container.transaction.Transactional;

public interface TestService2 {

	@Loggable
	@Secure
	@Transactional
	void test();
}
