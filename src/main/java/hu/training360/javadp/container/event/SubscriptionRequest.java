package hu.training360.javadp.container.event;

import java.lang.reflect.Method;

public class SubscriptionRequest {

	private Method method;
	private Object object;
	private String address;

	public SubscriptionRequest(Method method, Object object, String address) {
		super();
		this.method = method;
		this.object = object;
		this.address = address;
	}

	public Method getMethod() {
		return method;
	}

	public Object getObject() {
		return object;
	}

	public String getAddress() {
		return address;
	}

}
