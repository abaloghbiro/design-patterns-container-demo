package hu.training360.javadp.container.event;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Optional;

import hu.training360.javadp.container.common.Component;
import hu.training360.training.javadp.container.factory.Inject;

@Component
public class EventSubscriberService {

	@Inject
	private EventBus bus;

	public void subscribeBeanToEventbus(Object targetObject) {

		Optional<SubscriptionRequest> request = getSubscriptionRequest(targetObject);

		if (request.isPresent()) {
			bus.subScribe(request.get());
		}
	}

	private Optional<SubscriptionRequest> getSubscriptionRequest(Object targetObject) {

		SubscriptionRequest request = null;

		for (Method m : targetObject.getClass().getMethods()) {

			Annotation[][] annotations = m.getParameterAnnotations();

			for (int i = 0; i < annotations.length; i++) {

				for (int j = 0; j < annotations[i].length; j++) {

					Annotation annotation = annotations[i][j];

					if (annotation != null && annotation.annotationType().isAssignableFrom(Observer.class)) {
						Observer observer = (Observer) annotation;
						request = new SubscriptionRequest(m, targetObject, observer.address());
						break;
					}
				}
			}
			if (request != null) {
				break;
			}
		}

		return Optional.ofNullable(request);

	}

}
