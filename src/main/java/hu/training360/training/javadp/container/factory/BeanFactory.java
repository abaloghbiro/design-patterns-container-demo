package hu.training360.training.javadp.container.factory;

public interface BeanFactory {

	<T extends Object> T getBean(Class<T> beanClazz);
}
