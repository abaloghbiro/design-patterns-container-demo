package hu.training360.javadp.container.transaction;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

import hu.training360.javadp.container.common.AbstractProxyFactoryBean;
import hu.training360.javadp.container.common.Component;

@Component
public class DefaultTransactionalProxyFactoryBean extends AbstractProxyFactoryBean {

	@Override
	protected InvocationHandler getProxyHandler(Object targetObject) {
		return new InvocationHandler() {

			public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

				Object ret = null;
				if (method.getAnnotation(Transactional.class) != null) {

					System.out.println("Transaction started....");

					ret = method.invoke(targetObject, args);

					System.out.println("Transaction finished....");
				} else {
					ret = method.invoke(targetObject, args);
				}

				return ret;
			}

		};
	}

}
