package hu.training360.javadp.container.event;

public interface EventBus {

	void subScribe(SubscriptionRequest request);
	
	void unSubscribe(String address);
	
	void sendMessage(String address, Event<?> m);
	
	void sendMessage(Event<?> m);
}
