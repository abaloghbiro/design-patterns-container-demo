package hu.training360.javadp.container.common;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import hu.training360.training.javadp.container.bean.Scope;

@Documented
@Retention(RUNTIME)
@Target(ElementType.TYPE)
public @interface Component {

	Scope scope() default Scope.SINGLETON;

	String name() default "";
}
