package hu.training360.javadp.container.security;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import hu.training360.javadp.container.common.AbstractProxyFactoryBean;
import hu.training360.javadp.container.common.Component;
import hu.training360.javadp.container.security.decision.manager.AccessDecisionManager;
import hu.training360.javadp.container.security.strategy.DecisionStrategy;
import hu.training360.training.javadp.container.factory.Inject;

@Component
public class DefaultSecurityProxyFactoryBean extends AbstractProxyFactoryBean {

	@Inject
	private AccessDecisionManager dm;

	@Inject
	private DecisionStrategy st;

	@Override
	protected InvocationHandler getProxyHandler(Object targetObject) {
		return new InvocationHandler() {

			public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

				Object ret = null;

				if (isProxy(targetObject.getClass())) {
					System.out.println("ProxyFactory bean received an other proxy object...");
				}

				if (method.getAnnotation(Secure.class) != null) {

					System.out.println("Security check started....");

					SecurityContext sc = new SecurityContext();
					sc.setInvokedMethod(method);
					sc.setInvokedService(proxy);
					sc.setRole("admin");
					sc.setUsername("elek");

					if (dm.isAccessGranted(st, sc)) {
						ret = method.invoke(targetObject, args);
						System.out.println("Security check finished....");
					} else {
						throw new SecurityException("Access denied!");
					}

				} else {
					ret = method.invoke(targetObject, args);
				}

				return ret;
			}

		};
	}

}
