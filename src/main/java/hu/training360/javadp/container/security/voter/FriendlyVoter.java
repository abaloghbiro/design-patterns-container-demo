package hu.training360.javadp.container.security.voter;

import hu.training360.javadp.container.security.SecurityContext;

public class FriendlyVoter implements Voter {

	@Override
	public VoterDecision vote(SecurityContext sc) {
		return VoterDecision.APPROVED;
	}

}
