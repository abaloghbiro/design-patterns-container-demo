package hu.training360.javadp.container.event;

import java.util.Date;

public abstract class Event<V> {

	private Object source;
	private Date timestamp;
	private V payload;

	public Event(Object source, Date timestamp, V payload) {
		this.source = source;
		this.timestamp = timestamp;
		this.payload = payload;
	}

	public Object getSource() {
		return source;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public V getPayload() {
		return payload;
	}

	public void setPayload(V payload) {
		this.payload = payload;
	}

	@Override
	public String toString() {
		return "Event [source=" + source + ", timestamp=" + timestamp + "]";
	}

}
