package hu.training360.javadp.container.test;

import hu.training360.javadp.container.audit.Loggable;
import hu.training360.javadp.container.common.Component;
import hu.training360.javadp.container.event.Event;
import hu.training360.javadp.container.event.EventSubscriberService;
import hu.training360.javadp.container.event.Observer;
import hu.training360.javadp.container.security.Secure;
import hu.training360.javadp.container.transaction.Transactional;
import hu.training360.training.javadp.container.factory.Inject;

@Component
public class DefaultTestService implements TestService {

	private String data;

	@Inject
	private TestService2 testService;
	
	@Inject
	private EventSubscriberService subscriberService;

	public String getData() {
		return data;
	}

	@Loggable
	@Secure
	@Transactional
	public void setData(String data) {
		subscriberService.subscribeBeanToEventbus(this);
		this.data = data;
		testService.test();
		try {
			// TODO remove this line
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void listener(@Observer(address = "defaultTestService") Event<?> event) {

		System.out.println("Event received at: " + event.getTimestamp());
	}

	public TestService2 getTestService() {
		return testService;
	}

	public void setTestService(TestService2 testService) {
		this.testService = testService;
	}

}
