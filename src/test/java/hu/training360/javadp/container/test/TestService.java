package hu.training360.javadp.container.test;

import hu.training360.javadp.container.audit.Loggable;
import hu.training360.javadp.container.transaction.Transactional;

public interface TestService {

	
	@Transactional
	@Loggable
	void setData(String data);
}
