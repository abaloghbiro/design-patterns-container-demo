package hu.training360.javadp.container.audit;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

import hu.training360.javadp.container.common.AbstractProxyFactoryBean;
import hu.training360.javadp.container.common.Component;

@Component
public class DefaultLoggerProxyFactoryBean extends AbstractProxyFactoryBean {

	@Override
	protected InvocationHandler getProxyHandler(Object targetObject) {
		
		return new InvocationHandler() {

			public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

				Object ret = null;

				if (method.getAnnotation(Loggable.class) != null) {

					System.out.println("Log check started....");

					ret = method.invoke(targetObject, args);

					System.out.println("Log check finished....");
				} else {
					ret = method.invoke(targetObject, args);
				}

				return ret;
			}

		};
	}

}
