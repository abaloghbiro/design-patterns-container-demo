package hu.training360.javadp.container.common;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public abstract class AbstractProxyFactoryBean implements ProxyFactoryBean {

	protected ProxyFactoryBean nextProxyFactoryBean;

	@Override
	public Object createProxy(Object targetObject) {

		Object myProxy = getProxyObjectInstance(targetObject);

		if (nextProxyFactoryBean != null) {
			return nextProxyFactoryBean.createProxy(myProxy);
		} else {
			return myProxy;
		}

	}

	protected Object getProxyObjectInstance(Object targetObject) {
		return Proxy.newProxyInstance(targetObject.getClass().getClassLoader(), targetObject.getClass().getInterfaces(),
				getProxyHandler(targetObject));
	}

	protected abstract InvocationHandler getProxyHandler(Object targetObject);

	public ProxyFactoryBean getNextProxyFactoryBean() {
		return nextProxyFactoryBean;
	}

	public void setNextProxyFactoryBean(ProxyFactoryBean nextProxyFactoryBean) {
		this.nextProxyFactoryBean = nextProxyFactoryBean;
	}


	protected boolean isProxy(Class<?> proxyClass) throws Exception {
		if (Proxy.isProxyClass(proxyClass)) {
			return true;
		}
		return false;
	}

}
