package hu.training360.javadp.container.common;

public interface ProxyFactoryBean {

	Object createProxy(final Object targetObject);
}
