package hu.training360.training.javadp.container.bean;

public enum Scope {

	SINGLETON, PROTOTYPE;
}
