package hu.training360.javadp.container.security.voter;

import hu.training360.javadp.container.security.SecurityContext;

public class RoleBasedVoter implements Voter {

	@Override
	public VoterDecision vote(SecurityContext sc) {

		String role = sc.getRole();

		if (role == null) {
			return VoterDecision.IGNORED;
		} else {
			return role.equalsIgnoreCase("admin") ? VoterDecision.APPROVED : VoterDecision.DENIED;
		}
	}

}
