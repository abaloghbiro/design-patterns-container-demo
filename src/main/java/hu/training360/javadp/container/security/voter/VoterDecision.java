package hu.training360.javadp.container.security.voter;

public enum VoterDecision {

	APPROVED, DENIED, IGNORED;
}
