package hu.training360.javadp.container.event;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

import hu.training360.javadp.container.common.Component;

@Component
public class QueueBasedEventBus implements EventBus {

	private static final List<SubscriptionRequest> OBSERVERS = new ArrayList<>();

	private static final Queue<Doublet<String, Event<?>>> MESSAGES = new LinkedBlockingQueue<>();

	private Timer timer;

	public QueueBasedEventBus() {
		timer = new Timer(true);
		timer.scheduleAtFixedRate(new MessageSenderTask(), 0, 1000);
	}

	@Override
	public void subScribe(SubscriptionRequest request) {

		if (!OBSERVERS.contains(request)) {
			OBSERVERS.add(request);
		} else {
			throw new IllegalStateException("Observer with name already registered!");
		}

	}

	@Override
	public void unSubscribe(String address) {

		Optional<SubscriptionRequest> request = findSubscriptionByAddress(address);

		if (request.isPresent()) {
			OBSERVERS.remove(request.get());
		}
	}

	@Override
	public void sendMessage(String address, Event<?> m) {

		Doublet<String, Event<?>> d = new Doublet<>();
		d.setKey(address);
		d.setValue(m);

		MESSAGES.offer(d);
	}

	@Override
	public void sendMessage(Event<?> m) {
		Doublet<String, Event<?>> d = new Doublet<>();
		d.setValue(m);
		MESSAGES.offer(d);

	}

	private static final class Doublet<K, V> {

		private K key;
		private V value;

		public K getKey() {
			return key;
		}

		public void setKey(K key) {
			this.key = key;
		}

		public V getValue() {
			return value;
		}

		public void setValue(V value) {
			this.value = value;
		}
	}

	private Optional<SubscriptionRequest> findSubscriptionByAddress(String address) {

		List<SubscriptionRequest> observers = OBSERVERS.stream().filter(o -> o.getAddress().equalsIgnoreCase(address))
				.collect(Collectors.toList());

		if (observers.size() != 1) {
			throw new IllegalArgumentException("Invalid address!");
		}

		return Optional.ofNullable(observers.get(0));
	}

	private void deliverMessage(SubscriptionRequest request, Event<?> event) {

		Method m = request.getMethod();
		Object o = request.getObject();

		try {
			m.invoke(o, event);
		} catch (Exception e) {
			System.err.println("Fatal error during message sending: " + e.getMessage());
			e.printStackTrace();
		}
	}

	private class MessageSenderTask extends TimerTask {

		@Override
		public void run() {

			Doublet<String, Event<?>> queueEntry = MESSAGES.poll();

			if (queueEntry != null) {

				System.out.println("Event processing: " + queueEntry.getValue());

				if (queueEntry.getKey() != null) {

					Optional<SubscriptionRequest> request = findSubscriptionByAddress(queueEntry.getKey());

					if (request.isPresent()) {

						deliverMessage(request.get(), queueEntry.getValue());
					}
				} else {

					OBSERVERS.stream().forEach(o -> deliverMessage(o, queueEntry.getValue()));
				}
			}

		}

	}

}
