package hu.training360.javadp.container.security.strategy;

import java.util.List;

import hu.training360.javadp.container.security.voter.VoterDecision;

public interface DecisionStrategy {

	boolean isGranted(List<VoterDecision> decisions);
}
