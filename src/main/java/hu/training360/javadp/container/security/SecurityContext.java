package hu.training360.javadp.container.security;

import java.lang.reflect.Method;
import java.time.LocalDateTime;

public class SecurityContext {

	private String username;
	private String role;
	private Method invokedMethod;
	private Object invokedService;
	private LocalDateTime timestamp;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Method getInvokedMethod() {
		return invokedMethod;
	}

	public void setInvokedMethod(Method invokedMethod) {
		this.invokedMethod = invokedMethod;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public Object getInvokedService() {
		return invokedService;
	}

	public void setInvokedService(Object invokedService) {
		this.invokedService = invokedService;
	}

}
