package hu.training360.javadp.container.security.decision.manager;

import hu.training360.javadp.container.security.SecurityContext;
import hu.training360.javadp.container.security.strategy.DecisionStrategy;

public interface AccessDecisionManager {

	boolean isAccessGranted(DecisionStrategy strategy, SecurityContext sc);
}
