package hu.training360.javadp.container.security.voter;

import hu.training360.javadp.container.security.SecurityContext;

public interface Voter {

	VoterDecision vote(SecurityContext sc);
}
