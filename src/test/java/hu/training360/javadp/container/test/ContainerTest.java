package hu.training360.javadp.container.test;

import org.junit.Assert;
import org.junit.Test;

import hu.training360.training.javadp.container.factory.DefaultBeanFactory;

public class ContainerTest {

	@Test
	public void testBeanCreation() {

		DefaultBeanFactory factory = new DefaultBeanFactory();

		TestService beanObject = factory.getBean(DefaultTestService.class);

		Assert.assertNotNull(beanObject);

		TestService proxy = factory.getProxiedObject(beanObject);

		Assert.assertNotNull(proxy);
		
		Assert.assertNotNull(((DefaultTestService)beanObject).getTestService());

		proxy.setData("alma");

	}
}
